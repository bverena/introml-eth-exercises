#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# import modules
import numpy as np
import keras
import pandas as pd
from sklearn.utils import class_weight
from keras.models import Sequential # stack of linear hidden layers
from keras.layers import Conv2D, MaxPooling2D # 2D Is enough if input layer has depth 1
from keras.layers import Dense # Fully connected layer
from keras.layers import Dropout # Dropout regularisation
from keras.layers import Activation # Specify activation function
from keras.layers import Flatten # After Convolution and Pooling need to flatten
from keras.optimizers import SGD
from datetime import datetime
from keras import backend as K # check backend: can be tensorflor or tiano or other

#np.random.seed(100)
user = 'veri'
annotate = 'class weight auto'

# import data
train = pd.read_hdf("../train.h5", "train")
sub = pd.read_hdf("../test.h5", "test")

y = train.values[:,0] 
X = train.values[:,1:]
sub_idx = sub.index.values
sub_X = sub.values

# split into test and training data
fr = int(1/4.*len(y))
y_test = y[:fr]
y_train = y[fr:]
X_test = X[:fr,:]
X_train = X[fr:,:]

# get class weights
classes = np.unique(y_train).astype(int)
class_weights = class_weight.compute_class_weight('balanced', np.unique(y_train), y_train)
class_weights = dict(zip(classes, class_weights))

# output to categorical
y_train = keras.utils.to_categorical(y_train,5) 
y_test = keras.utils.to_categorical(y_test,5)

# NN
NN = Sequential()
NN.name = 'codersNN'

# settings
batch_size = 300
epochs = 200 # number of times to loop over whole data # shuffle when >1

learning_rate = 0.01
momentum = 0.9
decay = 1e-6

# optimizer
sgd = SGD(lr=learning_rate, decay=decay, momentum=momentum)

# callback
monitor = 'accuracy'
#monitor = 'val_loss'
earlyStopping = keras.callbacks.EarlyStopping(monitor=monitor, patience=0, verbose=1, mode='auto')

# set up neural network
NN.add(Dense(500, activation='relu', input_shape=(100,)))
NN.add(Dropout(0.5))
NN.add(Dense(200, activation='relu'))
NN.add(Dense(100, activation='relu'))
#NN.add(Dense(10, activation='relu'))
NN.add(Dense(5, activation='softmax'))

# set up model with settings
NN.compile(loss='categorical_crossentropy',
              optimizer=keras.optimizers.Adagrad(),
              metrics=['accuracy'])

# fit model
NN.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_data=(X_test, y_test), callbacks=[earlyStopping], class_weight='auto', shuffle=True)
#NN.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_data=(X_test, y_test), class_weight=class_weights)

# evaluate it on the test data
score = NN.evaluate(X_test, y_test, verbose=0)

# print loss and accuracy
print('loss {}, accuracy {}'.format(score[0], score[1]))

# predict
y_predict = NN.predict(sub_X)
y_predict = y_predict.argmax(1)

# save submission file
output = np.zeros((sub_X.shape[0],2))
output[:,0] = sub_idx
output[:,1] = y_predict

mode = 'test'
np.savetxt('submission_{}.csv'.format(mode), output, fmt='%10.8f', delimiter=',', header = "Id,y", comments = '')

# log what you've just done
with open('../log.txt', 'a') as f:
    logstring = 'time {}, acc {}, loss {}, user {}, fr_test {}, batch_size {}, epochs {}, learning rate {}, momentum {}, decay {}, annotations {}'.format(datetime.now(), score[1], score[0], user, fr, batch_size, epochs, learning_rate, momentum, decay, annotate)
    f.write(logstring + ' \n')

import gc; gc.collect() # for bug

## dropout layer (parameter: prob of dropout)
#CNN.add(Dropout(0.1)) # always to specific layer
#
## fully connected layer
#CNN.add(Flatten()) # necessary to connect to fully connected layer
## arguments: 1: number of ouputs, activation function 
#CNN.add(Dense(100, activation='relu')) # first argument no of outputs
## softmax indicates how to select one of the 10 options for the prediction output
#CNN.add(Dense(10, activation='softmax')) # since we have 10 different classes
#
## compile it
## arguments: loss function, optimization method (eg SGC), score metrics
#CNN.compile(loss='categorical_crossentropy', optimizer=keras.optimizers.Adadelta(),
#        metrics=['accuracy'])

