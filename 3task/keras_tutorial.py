import numpy as np
import tensorflow as tf
import keras
from keras.models import Sequential # stack of linear hidden layers
from keras.layers import Conv2D, MaxPooling2D # 2D Is enough if input layer has depth 1
from keras.layers import Dense # Fully connected layer
from keras.layers import Dropout # Dropout regularisation
from keras.layers import Activation # Specify activation function
from keras.layers import Flatten # After Convolution and Pooling need to flatten
from keras.datasets import mnist
from keras import backend as K # check backend

np.random.seed(100)

(x_train, y_train), (x_test, y_test) = mnist.load_data()
print(x_train.shape)

# PREPROCESSING
if K.image_data_format() == 'channels_first':
    input_shape = (1,28,28)
else:
    input_shape = (28,28,1)

print(y_train.shape)
# shape is (60000,) but we want a categorical vector for each label instead of the label
y_train = keras.utils.to_categorical(y_train,10) # 10 is the number of classes
y_test = keras.utils.to_categorical(y_test,10) # 10 is the number of classes
print(y_train.shape)

x_train = x_train.reshape(x_train.shape[0], *input_shape)
x_test = x_test.reshape(x_test.shape[0], *input_shape)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255


# CNN
CNN = Sequential()
CNN.name = 'CNN'

# convolution layers
# input layer (32 filters in convolution layer all of them of size 2*2 with
# activation relu
CNN.add(Conv2D(32, kernel_size=(2,2), activation='relu', input_shape = input_shape))
print(CNN.output_shape) # 27*27*32
# figures out connection automatically, therefore no input_shape necessary anymore
CNN.add(Conv2D(32, kernel_size=(3,3), activation='tanh'))

# pooling layer
CNN.add(MaxPooling2D(pool_size=(3,3)))

# dropout layer (parameter: prob of dropout)
CNN.add(Dropout(0.1))

# fully connected layer
CNN.add(Flatten()) # necessary to connect to fcl
# arguments: 1: number of ouputs, activation function 
CNN.add(Dense(100, activation='relu'))
# softmax indicates how to select one of the 10 options for the prediction output
CNN.add(Dense(10, activation='softmax'))

# compile it
# arguments: loss function, optimization method, score metrics
CNN.compile(loss='categorical_crossentropy', optimizer=keras.optimizers.Adadelta(),
        metrics=['accuracy'])

# fit it
batch_size = 80
epochs = 1 # "a parameter you do not have to worry about for now" - How many times i go through all the training datapoints
hist = CNN.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, 
        validation_data=(x_test, y_test))
print(hist.history)
# evaluate it on the test data
score = CNN.evaluate(x_test, y_test, verbose=0)
# loss
print(score[0])
# accuracey
print(score[1])
