#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# import modules
import numpy as np
import keras
import pandas as pd
from keras.models import Sequential # stack of linear hidden layers
from keras.layers import Dense # Fully connected layer
from keras.layers import Dropout # Dropout regularisation
from keras.optimizers import SGD
from datetime import datetime
from sklearn.preprocessing import StandardScaler

user = 'gianna'
annotate = 'SGD_batchShuffle_for_real'

# import data
train = pd.read_hdf("../train.h5", "train")
sub = pd.read_hdf("../test.h5", "test")

y = train.values[:,0] 
X = train.values[:,1:]


## standardize data
scaler = StandardScaler()
#X = scaler.fit_transform(X)
#
sub_idx = sub.index.values
#sub_X = sub.values
#sub_X = scaler.transform(sub_X)


sub_X = sub.values
scaler.fit(np.concatenate((X, sub_X)))
X = scaler.transform(X)
sub_X = scaler.transform(sub_X)


# split into test and training data
fr = int(1/6.*len(y))
y_test = y[:fr]
y_train = y[fr:]
X_test = X[:fr,:]
X_train = X[fr:,:]

# output to categorical
y_train = keras.utils.to_categorical(y_train,5) 
y_test = keras.utils.to_categorical(y_test,5)

# NN
NN = Sequential()
NN.name = 'codersNN'

# settings
batch_size = 200
epochs = 500 # number of times to loop over whole data # shuffle when >1

learning_rate = 0.01
momentum = 0.9
decay = 1e-6

# optimizer
optimizer = SGD(lr=learning_rate, decay=decay, momentum=momentum)
#optimizer = keras.optimizers.Adadelta()
#optimizer = keras.optimizers.Adam()
#optimizer = keras.optimizers.Adagrad()
#optimizer = keras.optimizers.RMSprop()

# regularizer
monitor = 'val_acc'
earlyStopping = keras.callbacks.EarlyStopping(monitor=monitor, patience=20, verbose=1, mode='auto')
# about patience: "The patience is often set somewhere between 10 and 100 (10 or 20 is more common), but it really depends on your dataset and network." see https://stats.stackexchange.com/questions/231061/how-to-use-early-stopping-properly-for-training-deep-neural-network

# set up neural network
#NN.add(Dense(1000, activation='relu', input_shape=(100,)))
#NN.add(Dropout(0.5))
#NN.add(Dense(500, activation='relu', input_shape=(100,)))
#NN.add(Dropout(0.5))
NN.add(Dense(300, activation='relu', input_shape=(100,)))
NN.add(Dropout(0.5))
#NN.add(Dense(50, activation='relu'))
#NN.add(Dropout(0.1))
NN.add(Dense(30, activation='relu'))
#NN.add(Dense(10, activation='relu'))
NN.add(Dense(5, activation='softmax'))

# set up model with settings
NN.compile(loss='categorical_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])

# fit model
hist = NN.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1,
        validation_data=(X_test, y_test), callbacks=[earlyStopping], 
        class_weight='auto', shuffle='batch')

print(hist.history)
# evaluate it on the test data
score = NN.evaluate(X_test, y_test, verbose=0)

# print loss and accuracy
print('loss {}, accuracy {}'.format(score[0], score[1]))

# predict
y_predict = NN.predict(sub_X)
y_predict = y_predict.argmax(1)

# save submission file
output = np.zeros((sub_X.shape[0],2))
output[:,0] = sub_idx
output[:,1] = y_predict

np.savetxt('submission_{}_acc_{}.csv'.format(annotate,score[1]), output, fmt='%10.8f', delimiter=',', header = "Id,y", comments = '')

# log what you've just done
with open('../log.txt', 'a') as f:
    logstring = 'time {}, acc {}, loss {}, user {}, fr_test {}, batch_size {}, epochs {}, learning rate {}, momentum {}, decay {}, annotations {}'.format(datetime.now(), score[1], score[0], user, fr, batch_size, epochs, learning_rate, momentum, decay, annotate)
    f.write(logstring + ' \n')

import gc; gc.collect() # for bug
