#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# import modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.utils import class_weight
#import keras
#from keras.models import Sequential # stack of linear hidden layers
#from keras.layers import Conv2D, MaxPooling2D # 2D Is enough if input layer has depth 1
#from keras.layers import Dense # Fully connected layer
#from keras.layers import Dropout # Dropout regularisation
#from keras.layers import Activation # Specify activation function
#from keras.layers import Flatten # After Convolution and Pooling need to flatten
#from keras.optimizers import SGD
#from keras import backend as K # check backend: can be tensorflor or tiano or other
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.preprocessing import PolynomialFeatures
from sklearn.svm import LinearSVC, SVC
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score

sub_name = 'test'
#patience = 7
pca_n_predictors = 80
n_folds = 3
lambdas = np.logspace(-8,0,6)
poly_degree = 2
interaction_only = False

# import data
train_lab = pd.read_hdf("train_labeled.h5", "train")
train_unlab = pd.read_hdf("train_unlabeled.h5", "train")
sub = pd.read_hdf("test.h5", "test")

y_lab = train_lab.values[:,0] 
X_lab = train_lab.values[:,1:]
# unlabeled data
X_unlab = train_unlab.values[:,:]

sub_idx = sub.index.values
X_sub = sub.values

n_clusters = len(np.unique(y_lab))
n_predictors = X_lab.shape[1]

# combine labeled and unlabeled
X = np.append(X_lab, X_unlab, axis=0)
X = np.append(X, X_sub, axis = 0)

# Interaction terms
print(X_sub.shape)
print('Interaction')
pf = PolynomialFeatures(degree=poly_degree, interaction_only=interaction_only)
X = pf.fit_transform(X)
X_sub = pf.transform(X_sub)
X_lab = pf.transform(X_lab)
print(X_sub.shape)

# SCALE
print('Scale')
scaler = StandardScaler()
X = scaler.fit_transform(X)
X_lab = scaler.transform(X_lab)
X_sub = scaler.transform(X_sub)


print('PCA')
pca = PCA(pca_n_predictors, whiten=False)
X = pca.fit_transform(X)
X_sub = pca.transform(X_sub)
X_lab = pca.transform(X_lab)
print(pca.explained_variance_ratio_)
print(pca.explained_variance_)
print(X_sub.shape)

final_shape = X_sub.shape[1]
print(final_shape)


kf = KFold(n_splits=n_folds)

acc = np.zeros((n_folds, len(lambdas)))



print('train data shape: ', X_lab.shape)

for li, l in enumerate(lambdas): # lambdas

    print('#############')
    print('lambda', l)

    for fi, (cvtrain_idx, cvtest_idx) in enumerate(kf.split(X_lab)): #folds
        print(fi)

        # select test and training fold
        X_cvtrain = X_lab[cvtrain_idx,:]
        y_cvtrain = y_lab[cvtrain_idx]

        X_cvtest = X_lab[cvtest_idx,:]
        y_cvtest = y_lab[cvtest_idx]

        # initialize regression
        #classif = LinearSVC(C=l, class_weight='balanced')
        classif = SVC(kernel='poly', degree=3, C=l, class_weight='balanced')
        #classif = SVC(kernel='rbf', gamma='auto', C=l, class_weight='balanced')

        # fit regression to training data
        classif.fit(X_cvtrain, y_cvtrain)

        # predict from fitted regression
        y_predict = classif.predict(X_cvtest)

        # calculate rmse
        acc[fi,li] = accuracy_score(y_cvtest, y_predict)

    acc_lambda = np.mean(acc[:,li],0)
    print('acc: ',str(np.round(acc_lambda,4)))
    print('#############')

# Print mean accuracy over all folds for each lambda
acc_mean = np.mean(acc, axis=0)
print(lambdas)
print(acc_mean)


quit()


# predict
y_predict = svc.predict(X_sub)
#y_predict = y_predict.argmax(1)

# save submission file
output = np.zeros((X_sub.shape[0],2))
output[:,0] = sub_idx
output[:,1] = y_predict

np.savetxt('submission_{}.csv'.format(sub_name), output, fmt='%10.8f', delimiter=',', header = "Id,y", comments = '')


import gc; gc.collect() # for bug

