#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# import modules
import numpy as np
import keras
import pandas as pd
from sklearn.utils import class_weight
from keras.models import Sequential # stack of linear hidden layers
from keras.layers import Conv2D, MaxPooling2D # 2D Is enough if input layer has depth 1
from keras.layers import Dense # Fully connected layer
from keras.layers import Dropout # Dropout regularisation
from keras.layers import Activation # Specify activation function
from keras.layers import Flatten # After Convolution and Pooling need to flatten
from keras.optimizers import SGD
from keras import backend as K # check backend: can be tensorflor or tiano or other
from datetime import datetime
from sklearn.preprocessing import StandardScaler

sub_name = 'pseudo_labeling_new_3'
patience = 2

# import data
train_lab = pd.read_hdf("train_labeled.h5", "train")
train_unlab = pd.read_hdf("train_unlabeled.h5", "train")
sub = pd.read_hdf("test.h5", "test")

y_lab = train_lab.values[:,0] 
X_lab = train_lab.values[:,1:]

n_clusters = len(np.unique(y_lab))
n_predictors = X_lab.shape[1]

# SCALE
scaler = StandardScaler()
X_lab = scaler.fit_transform(X_lab)

sub_idx = sub.index.values
sub_X = sub.values
sub_X = scaler.transform(sub_X)

# unlabeled data
X_unlab = train_unlab.values[:,:]
X_unlab = scaler.transform(X_unlab)

# split into test and training data
fr = int(1/100.*len(y_lab))
y_test = y_lab[:fr]
y_train = y_lab[fr:]
X_test = X_lab[:fr,:]
X_train = X_lab[fr:,:]

# output to categorical
y_train = keras.utils.to_categorical(y_train,n_clusters) 
y_test = keras.utils.to_categorical(y_test,n_clusters)


# NN
NN = Sequential()
NN.name = 'codersNN'

# settings
batch_size = 500
epochs = 30 # number of times to loop over whole data # shuffle when >1

learning_rate = 0.001
momentum = 0.9
decay = 1e-6

# optimizer
optimizer = SGD(lr=learning_rate, decay=decay, momentum=momentum)
optimizer = keras.optimizers.Adadelta()
#optimizer = keras.optimizers.Adam()
#optimizer = keras.optimizers.Adagrad()
#optimizer = keras.optimizers.RMSprop()

# callback
monitor = 'val_loss'
monitor = 'val_acc'
monitor = 'acc'
earlyStopping = keras.callbacks.EarlyStopping(monitor=monitor, patience=patience,
        verbose=1, mode='auto')

# set up neural network
NN.add(Dense(1000, activation='relu', input_shape=(n_predictors,)))
NN.add(Dropout(0.5))
NN.add(Dense(500, activation='relu'))
NN.add(Dropout(0.5))
NN.add(Dense(300, activation='relu'))
NN.add(Dropout(0.5))
NN.add(Dense(50, activation='relu'))
NN.add(Dense(30, activation='relu'))
NN.add(Dense(10, activation='softmax'))

# set up model with settings
NN.compile(loss='categorical_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])

# fit model on labeled data
NN.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1,
        validation_data=(X_test, y_test), callbacks=[earlyStopping], 
        class_weight='auto', shuffle=True)

# evaluate it on the test data
score = NN.evaluate(X_test, y_test, verbose=0)

# print loss and accuracy
print('loss {}, accuracy {}'.format(score[0], score[1]))

# create pseudo labeled data
y_unlab = NN.predict(X_unlab)
y_unlab = np.argmax(y_unlab, axis=1)


y_train = np.argmax(y_train, axis=1)
y_train = np.append(y_train, y_unlab)
X_train = np.append(X_train, X_unlab, axis=0)

y_train = keras.utils.to_categorical(y_train,n_clusters) 

batch_size = 500
patience = 8
#monitor = 'val_loss'
monitor = 'val_acc'
earlyStopping = keras.callbacks.EarlyStopping(monitor=monitor, patience=patience,
        verbose=1, mode='auto')
NN.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1,
        validation_data=(X_test, y_test), callbacks=[earlyStopping], 
        class_weight='auto', shuffle=True)

y_predict = NN.predict(sub_X)
y_predict = y_predict.argmax(1)


# save submission file
output = np.zeros((sub_X.shape[0],2))
output[:,0] = sub_idx
output[:,1] = y_predict

np.savetxt('submission_{}.csv'.format(sub_name), output, fmt='%10.8f', delimiter=',', header = "Id,y", comments = '')




quit()
split_ind = 9000

y_all = np.append(y_lab[:split_ind], y_unlab)
X_all = np.append(X_lab[:split_ind,:], X_unlab, axis=0)
#print(X_all.shape)
#print(y_all.shape)


## fit model on pseudo-labeled data
#fr = int(1/6.*len(y_all))
#y_test = y_all[:fr]
#y_train = y_all[fr:]
#X_test = X_all[:fr,:]
#X_train = X_all[fr:,:]

X_train = X_all
y_train = y_all
X_eval = X_lab[split_ind:,]
y_eval = y_lab[split_ind:,]
y_train = keras.utils.to_categorical(y_train,n_clusters) 
y_eval = keras.utils.to_categorical(y_eval,n_clusters)

# output to categorical
batch_size = 500
patience = 3
monitor = 'acc'
earlyStopping = keras.callbacks.EarlyStopping(monitor=monitor, patience=patience,
        verbose=1, mode='auto')
NN.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1,
        validation_data=(X_eval, y_eval), callbacks=[earlyStopping], 
        class_weight='auto', shuffle=True)

# predict
y_predict = NN.predict(sub_X)
y_predict = y_predict.argmax(1)

# save submission file
output = np.zeros((sub_X.shape[0],2))
output[:,0] = sub_idx
output[:,1] = y_predict

np.savetxt('submission_{}.csv'.format(sub_name), output, fmt='%10.8f', delimiter=',', header = "Id,y", comments = '')


import gc; gc.collect() # for bug

