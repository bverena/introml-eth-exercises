#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# import modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.utils import class_weight
import keras
from keras.models import Sequential # stack of linear hidden layers
from keras.layers import Conv2D, MaxPooling2D # 2D Is enough if input layer has depth 1
from keras.layers import Dense # Fully connected layer
from keras.layers import Dropout # Dropout regularisation
from keras.layers import Activation # Specify activation function
from keras.layers import Flatten # After Convolution and Pooling need to flatten
from keras.optimizers import SGD
from keras import backend as K # check backend: can be tensorflor or tiano or other
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA, KernelPCA
from sklearn.preprocessing import PolynomialFeatures

sub_name = 'test'
patience = 7
kpca_n_predictors = 30
pca_n_predictors = 100

# import data
train_lab = pd.read_hdf("train_labeled.h5", "train")
train_unlab = pd.read_hdf("train_unlabeled.h5", "train")
sub = pd.read_hdf("test.h5", "test")

y_lab = train_lab.values[:,0] 
X_lab = train_lab.values[:,1:]
# unlabeled data
X_unlab = train_unlab.values[:,:]

sub_idx = sub.index.values
X_sub = sub.values

n_clusters = len(np.unique(y_lab))
n_predictors = X_lab.shape[1]

# combine labeled and unlabeled
X = np.append(X_lab, X_unlab, axis=0)
X = np.append(X, X_sub, axis = 0)

# Interaction terms
print(X_sub.shape)
pf = PolynomialFeatures(degree=2, interaction_only=True)
X = pf.fit_transform(X)
X_sub = pf.transform(X_sub)
X_lab = pf.transform(X_lab)
print(X_sub.shape)

# SCALE
scaler = StandardScaler()
X = scaler.fit_transform(X)
X_lab = scaler.transform(X_lab)
X_sub = scaler.transform(X_sub)
#X_unlab = scaler.transform(X_unlab)

#split_ind = 9000
#X_lab = X_lab[:split_ind,:]
#y_lab = y_lab[:split_ind]

#kpca = KernelPCA(n_components=kpca_n_predictors, kernel='rbf')
#kpca.fit(X)
#X_lab = kpca.transform(X_lab)
#X_sub = kpca.transform(X_sub)

print(X_sub.shape)
pca = PCA(pca_n_predictors, whiten=False)
X = pca.fit_transform(X)
X_sub = pca.transform(X_sub)
X_lab = pca.transform(X_lab)
print(pca.explained_variance_ratio_)
print(X_sub.shape)

final_shape = X_sub.shape[1]
print(final_shape)

## combine labeled and unlabeled
#X = np.append(X_lab, X_unlab, axis=0)

### plot
#i = 0
#plt.scatter(X_lab[:,i],X_lab[:,i+1],c=y_lab, cmap=plt.cm.jet)
#plt.show()
#quit()

# split into test and training data
fr = int(1/6.*len(y_lab))
y_test = y_lab[:fr]
y_train = y_lab[fr:]
X_test = X_lab[:fr,:]
X_train = X_lab[fr:,:]

# output to categorical
y_train = keras.utils.to_categorical(y_train,n_clusters) 
y_test = keras.utils.to_categorical(y_test,n_clusters)


# NN
NN = Sequential()
NN.name = 'codersNN'

# settings
batch_size = 500
epochs = 500 # number of times to loop over whole data # shuffle when >1

learning_rate = 0.001
momentum = 0.9
decay = 1e-6

# optimizer
optimizer = SGD(lr=learning_rate, decay=decay, momentum=momentum)
#optimizer = keras.optimizers.Adadelta()
optimizer = keras.optimizers.Adam()
#optimizer = keras.optimizers.Adagrad()
#optimizer = keras.optimizers.RMSprop()

# callback
monitor = 'val_loss'
monitor = 'val_acc'
#monitor = 'accuracy'
earlyStopping = keras.callbacks.EarlyStopping(monitor=monitor, patience=patience,
        verbose=1, mode='auto')

# set up neural network
NN.add(Dense(1000, activation='relu', input_shape=(final_shape,)))
NN.add(Dropout(0.5))
NN.add(Dense(500, activation='relu'))
NN.add(Dropout(0.5))
NN.add(Dense(300, activation='relu'))
NN.add(Dropout(0.5))
NN.add(Dense(50, activation='relu'))
NN.add(Dense(30, activation='relu'))
NN.add(Dense(10, activation='softmax'))

# set up model with settings
NN.compile(loss='categorical_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])

# fit model on labeled data
NN.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1,
        validation_data=(X_test, y_test), callbacks=[earlyStopping], 
        class_weight='auto', shuffle=True)

# evaluate it on the test data
score = NN.evaluate(X_test, y_test, verbose=0)

# print loss and accuracy
print('loss {}, accuracy {}'.format(score[0], score[1]))

## create pseudo labeled data
#y_unlab = NN.predict(X_unlab)
#y_unlab = np.argmax(y_unlab, axis=1)
#
#split_ind = 9000
#
#y_all = np.append(y_lab[:split_ind], y_unlab)
#X_all = np.append(X_lab[:split_ind,:], X_unlab, axis=0)
##print(X_all.shape)
##print(y_all.shape)
#
#
### fit model on pseudo-labeled data
##fr = int(1/6.*len(y_all))
##y_test = y_all[:fr]
##y_train = y_all[fr:]
##X_test = X_all[:fr,:]
##X_train = X_all[fr:,:]
#
#X_train = X_all
#y_train = y_all
#X_eval = X_lab[split_ind:,]
#y_eval = y_lab[split_ind:,]
#y_train = keras.utils.to_categorical(y_train,n_clusters) 
#y_eval = keras.utils.to_categorical(y_eval,n_clusters)
#
## output to categorical
#batch_size = 500
#patience = 3
#monitor = 'acc'
#earlyStopping = keras.callbacks.EarlyStopping(monitor=monitor, patience=patience,
#        verbose=1, mode='auto')
#NN.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1,
#        validation_data=(X_eval, y_eval), callbacks=[earlyStopping], 
#        class_weight='auto', shuffle=True)

# predict
y_predict = NN.predict(X_sub)
y_predict = y_predict.argmax(1)

# save submission file
output = np.zeros((X_sub.shape[0],2))
output[:,0] = sub_idx
output[:,1] = y_predict

np.savetxt('submission_{}.csv'.format(sub_name), output, fmt='%10.8f', delimiter=',', header = "Id,y", comments = '')


import gc; gc.collect() # for bug

