#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 4: yolo
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# import modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random
#from sklearn import mixture
from sklearn import cluster
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score



# import data
train_lab = pd.read_hdf("train_labeled.h5", "train")
train_unlab = pd.read_hdf("train_unlabeled.h5", "train")
sub = pd.read_hdf("test.h5", "test")

#print(train_lab.shape)
#print(train_unlab.shape)
#print(sub.shape)
#quit()

# divide into X and y
y_lab = train_lab.values[:,0] 
X_lab = train_lab.values[:,1:]
#sub_idx = sub.index.values
#sub_X = sub.values

# scaling
X_lab = StandardScaler().fit_transform(X_lab)

# get number of labels from labelled dataset
n_clusters = len(np.unique(y_lab))
print(np.histogram(y_lab,bins=10))

## plot
#n_pred = X_lab.shape[1]
#i = random.randint(0,n_pred)
#j = random.randint(0,n_pred)
#plt.scatter(X_lab[:,i],X_lab[:,j],c=y_lab, cmap=plt.cm.jet)
#plt.show()
#quit()






quit()


# estimate
#####
## mixtures
# GaussianMixture 0.17
# BayesianGaussianMixture 0.08
## clusters
# Kmeans 0.18
# SpectralClustering 0.08
# AgglomerativeClustering
    # affinity euclidean, linkage complete 0.04
    # affinity euclidean, linkage ward 0.10
    # affinity euclidean, linkage average 0.10
    # affinity l1, linkage complete 0.19
    # affinity l1, linkage average 0.05
    # affinity l2, linkage complete 0.04
    # affinity l2, linkage average 0.10
    # affinity manhattan, linkage complete 0.19
    # affinity manhattan, linkage average 0.05
    # affinity cosine, linkage complete 0.07
    # affinity precomputed, linkage complete --> error
# Birch 0.13 (default)
#####

## mixtures
#estimator = mixture.GaussianMixture(n_components=n_clusters, covariance_type='full')
#estimator = mixture.BayesianGaussianMixture(n_components=n_clusters, covariance_type='full')
#estimator.fit(X_lab)
#y_estimated = estimator.predict(X_lab) # mixtures

## clusters
estimator = cluster.KMeans(n_clusters=n_clusters)
#estimator = cluster.SpectralClustering(n_clusters=n_clusters, eigen_solver='arpack',
#                                        affinity='nearest_neighbors')
#estimator = cluster.AgglomerativeClustering(n_clusters=n_clusters, affinity='manhattan',
#                                            linkage='average')
#estimator = cluster.Birch(n_clusters=n_clusters, threshold=0.5, branching_factor=30)
estimator.fit(X_lab)
y_estimated = estimator.labels_ # clusters
print(np.histogram(y_estimated,bins=10))

print(np.histogram(y_lab,bins=10))
quit()
accuracy = accuracy_score(y_lab, y_estimated)
print(accuracy)
quit()
# split into test and training data
fr = int(1/6.*len(y))
y_test = y[:fr]
y_train = y[fr:]
X_test = X[:fr,:]
X_train = X[fr:,:]

# get class weights
classes = np.unique(y_train).astype(int)
class_weights = class_weight.compute_class_weight('balanced', np.unique(y_train), y_train)
class_weights = dict(zip(classes, class_weights))

# output to categorical
y_train = keras.utils.to_categorical(y_train,5) 
y_test = keras.utils.to_categorical(y_test,5)

# NN
NN = Sequential()
NN.name = 'codersNN'

# settings
batch_size = 32
epochs = 1 # number of times to loop over whole data # shuffle when >1

learning_rate = 0.01
momentum = 0.9
decay = 1e-6

# optimizer
sgd = SGD(lr=learning_rate, decay=decay, momentum=momentum)

# callback
earlyStopping = keras.callbacks.EarlyStopping(monitor='val_loss', patience=0, verbose=1, mode='auto')

# set up neural network
NN.add(Dense(100, activation='relu', input_shape=(100,)))
NN.add(Dropout(0.5))
NN.add(Dense(10, activation='relu'))
NN.add(Dense(5, activation='softmax'))

# set up model with settings
NN.compile(loss='categorical_crossentropy',
              optimizer=sgd,
              metrics=['accuracy'])

# fit model
NN.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_data=(X_test, y_test), callbacks=[earlyStopping], class_weight=class_weights)

# evaluate it on the test data
score = NN.evaluate(X_test, y_test, verbose=0)

# print loss and accuracy
print('loss {}, accuracy {}'.format(score[0], score[1]))

# predict
y_predict = NN.predict(sub_X)
y_predict = y_predict.argmax(1)

# save submission file
output = np.zeros((sub_X.shape[0],2))
output[:,0] = sub_idx
output[:,1] = y_predict

mode = 'test'
np.savetxt('submission_{}.csv'.format(mode), output, fmt='%10.8f', delimiter=',', header = "Id,y", comments = '')

# log what you've just done
with open('log.txt', 'a') as f:
    logstring = 'acc {}, loss {}, user {}, fr_test {}, batch_size {}, epochs {}, learning rate {}, momentum {}, decay {}'.format(score[1], score[0], user, fr, batch_size, epochs, learning_rate, momentum, decay)
    f.write(logstring + ' \n')

import gc; gc.collect() # for bug

## dropout layer (parameter: prob of dropout)
#CNN.add(Dropout(0.1)) # always to specific layer
#
## fully connected layer
#CNN.add(Flatten()) # necessary to connect to fully connected layer
## arguments: 1: number of ouputs, activation function 
#CNN.add(Dense(100, activation='relu')) # first argument no of outputs
## softmax indicates how to select one of the 10 options for the prediction output
#CNN.add(Dense(10, activation='softmax')) # since we have 10 different classes
#
## compile it
## arguments: loss function, optimization method (eg SGC), score metrics
#CNN.compile(loss='categorical_crossentropy', optimizer=keras.optimizers.Adadelta(),
#        metrics=['accuracy'])

