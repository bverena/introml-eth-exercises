#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from scipy.stats import mode

# load data
print('Load')
train_lab = pd.read_hdf("train_labeled.h5", "train")
train_unlab = pd.read_hdf("train_unlabeled.h5", "train")
sub = pd.read_hdf("test.h5", "test")
print(train_lab.shape)
print(train_unlab.shape)
print(sub.shape)

# divide into test, train, X, y
y_lab = train_lab.values[:,0] 
X_lab = train_lab.values[:,1:]
X_unlab = train_unlab.values[:,:] # unlabeled

sub_idx = sub.index.values
X_sub = sub.values

X = np.append(X_lab, X_unlab, axis=0) # all train data
print(X.shape)

# Interaction terms TODO
#pf = PolynomialFeatures(degree=poly_degree, interaction_only=interaction_only)
#X = pf.fit_transform(X)
#X_sub = pf.transform(X_sub)
#X_lab = pf.transform(X_lab)

# scale
print('Scale')
scaler = StandardScaler()
#X = scaler.fit_transform(X)
X = scaler.fit_transform(X)
X_unlab = scaler.transform(X_unlab)
X_lab = scaler.transform(X_lab)
X_sub = scaler.transform(X_sub)

# dimensionality reduction
#pca_n_predictors = 25
#print('PCA')
#pca = PCA(pca_n_predictors, whiten=False)
#X = pca.fit_transform(X)
#X_unlab = pca.transform(X_unlab)
#X_sub = pca.transform(X_sub)
#X_lab = pca.transform(X_lab)
#print(np.sum(pca.explained_variance_ratio_))

# k-means clustering on unlabeled data to get labels
n_labels = len(np.unique(y_lab))
kmeans = KMeans(n_clusters=n_labels)
y_predict = kmeans.fit_predict(X)
np.save('y_predict.npy', y_predict)
y_predict = np.load('y_predict.npy')
y_lab_predict = y_predict[:9000]
y_unlab_predict = y_predict[9000:]
print(np.unique(y_lab[y_lab_predict == 0], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 1], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 2], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 3], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 4], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 5], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 6], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 7], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 8], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 9], return_counts=True)[:])

print(mode(y_lab[y_lab_predict == 0]))
print(mode(y_lab[y_lab_predict == 1]))
print(mode(y_lab[y_lab_predict == 2]))
print(mode(y_lab[y_lab_predict == 3]))
print(mode(y_lab[y_lab_predict == 4]))
print(mode(y_lab[y_lab_predict == 5]))
print(mode(y_lab[y_lab_predict == 6]))
print(mode(y_lab[y_lab_predict == 7]))
print(mode(y_lab[y_lab_predict == 8]))
print(mode(y_lab[y_lab_predict == 9]))

# hand made label matching between cluster result and labeled data
y_custom = np.full(y_unlab_predict.shape, np.nan)
y_custom[y_unlab_predict == 0] = 2
y_custom[y_unlab_predict == 1] = 1
y_custom[y_unlab_predict == 2] = 7
y_custom[y_unlab_predict == 3] = 4
y_custom[y_unlab_predict == 4] = 6
y_custom[y_unlab_predict == 5] = 8
y_custom[y_unlab_predict == 6] = 0
y_custom[y_unlab_predict == 7] = 1
y_custom[y_unlab_predict == 8] = 3
y_custom[y_unlab_predict == 9] = 5
