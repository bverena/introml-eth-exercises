#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports
import numpy as np
import pandas as pd
import keras
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from scipy.stats import mode
from sklearn.mixture import GaussianMixture
from keras.models import Sequential # stack of linear hidden layers
from keras.optimizers import SGD
from keras.layers import Dense # Fully connected layer
from keras.layers import Dropout # Dropout regularisation

# settings


# load data
print('Load')
train_lab = pd.read_hdf("train_labeled.h5", "train")
sub = pd.read_hdf("test.h5", "test")

# divide into test, train, X, y
y_lab = train_lab.values[:,0] 
X_lab = train_lab.values[:,1:]

sub_idx = sub.index.values
X_sub = sub.values
n_labels = len(np.unique(y_lab))

# Interaction terms TODO
#pf = PolynomialFeatures(degree=poly_degree, interaction_only=interaction_only)
#X = pf.fit_transform(X)
#X_sub = pf.transform(X_sub)
#X_lab = pf.transform(X_lab)

# scale
print('Scale')
scaler = StandardScaler()
X_lab = scaler.fit_transform(X_lab)
X_sub = scaler.transform(X_sub)

# dimensionality reduction
pca_n_predictors = 100
print('PCA')
pca = PCA(pca_n_predictors, whiten=False)
X_lab = pca.fit_transform(X_lab)
X_sub = pca.transform(X_sub)
print(np.sum(pca.explained_variance_ratio_))

# neural network only on labeled data
y_lab = keras.utils.to_categorical(y_lab,n_labels) 
y_test = y_lab[:1000]
y_train = y_lab[1000:]
X_test = X_lab[:1000,:]
X_train = X_lab[1000:,:]

# NN
NN = Sequential()
NN.name = 'codersNN'

# settings
batch_size = 500
epochs = 500 # number of times to loop over whole data # shuffle when >1

learning_rate = 0.001
momentum = 0.9
decay = 1e-6

# optimizer
optimizer = SGD(lr=learning_rate, decay=decay, momentum=momentum)

# callback
monitor = 'val_acc'
earlyStopping = keras.callbacks.EarlyStopping(monitor=monitor, patience=25,
        verbose=1, mode='auto')

# set up neural network
NN.add(Dense(1000, activation='relu', input_shape=(pca_n_predictors,)))
NN.add(Dropout(0.5))
NN.add(Dense(500, activation='relu'))
NN.add(Dropout(0.5))
NN.add(Dense(300, activation='relu'))
NN.add(Dropout(0.5))
NN.add(Dense(50, activation='relu'))
#NN.add(Dropout(0.1))
NN.add(Dense(30, activation='relu'))
NN.add(Dense(10, activation='softmax'))

# set up model with settings
NN.compile(loss='categorical_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])

# fit model
NN.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1,
        validation_data=(X_test, y_test), callbacks=[earlyStopping], 
        class_weight='auto', shuffle=True)

# evaluate it on the test data
score = NN.evaluate(X_test, y_test, verbose=0)

print('loss {}, accuracy {}'.format(score[0], score[1]))

# predict labels
y_sub = NN.predict(X_sub)
y_sub = np.argmax(y_sub, axis=1)

# save submission file
output = np.zeros((X_sub.shape[0],2))
output[:,0] = sub_idx
output[:,1] = y_sub

mode = 'nn_only'
np.savetxt('submission_{}.csv'.format(mode), output, fmt='%10.8f', delimiter=',', header = "Id,y", comments = '')
