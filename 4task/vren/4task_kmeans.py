#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports
import numpy as np
import pandas as pd
import keras
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from scipy.stats import mode
from sklearn.mixture import GaussianMixture
from keras.models import Sequential # stack of linear hidden layers
from keras.optimizers import SGD
from keras.layers import Dense # Fully connected layer
from keras.layers import Dropout # Dropout regularisation

# settings


# load data
print('Load')
train_lab = pd.read_hdf("train_labeled.h5", "train")
train_unlab = pd.read_hdf("train_unlabeled.h5", "train")
sub = pd.read_hdf("test.h5", "test")
print(train_lab.shape)
print(train_unlab.shape)
print(sub.shape)

# divide into test, train, X, y
y_lab = train_lab.values[:,0] 
X_lab = train_lab.values[:,1:]
X_unlab = train_unlab.values[:,:] # unlabeled

sub_idx = sub.index.values
X_sub = sub.values

X = np.append(X_lab, X_unlab, axis=0) # all train data
n_labels = len(np.unique(y_lab))
print(X.shape)

# Interaction terms TODO
#pf = PolynomialFeatures(degree=poly_degree, interaction_only=interaction_only)
#X = pf.fit_transform(X)
#X_sub = pf.transform(X_sub)
#X_lab = pf.transform(X_lab)

# scale
print('Scale')
scaler = StandardScaler()
#X = scaler.fit_transform(X)
X = scaler.fit_transform(X)
X_unlab = scaler.transform(X_unlab)
X_lab = scaler.transform(X_lab)
X_sub = scaler.transform(X_sub)

# dimensionality reduction
pca_n_predictors = 25
print('PCA')
pca = PCA(pca_n_predictors, whiten=False)
X = pca.fit_transform(X)
X_unlab = pca.transform(X_unlab)
X_sub = pca.transform(X_sub)
X_lab = pca.transform(X_lab)
print(np.sum(pca.explained_variance_ratio_))

# k-means clustering on unlabeled data to get labels
#kmeans = KMeans(n_clusters=n_labels)
#y_predict = kmeans.fit_predict(X)
#np.save('y_predict.npy', y_predict)
y_predict = np.load('y_predict.npy')
y_lab_predict = y_predict[:9000]
y_unlab_predict = y_predict[9000:]
print(np.unique(y_lab[y_lab_predict == 0], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 1], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 2], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 3], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 4], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 5], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 6], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 7], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 8], return_counts=True)[:])
print(np.unique(y_lab[y_lab_predict == 9], return_counts=True)[:])

print(mode(y_lab[y_lab_predict == 0]))
print(mode(y_lab[y_lab_predict == 1]))
print(mode(y_lab[y_lab_predict == 2]))
print(mode(y_lab[y_lab_predict == 3]))
print(mode(y_lab[y_lab_predict == 4]))
print(mode(y_lab[y_lab_predict == 5]))
print(mode(y_lab[y_lab_predict == 6]))
print(mode(y_lab[y_lab_predict == 7]))
print(mode(y_lab[y_lab_predict == 8]))
print(mode(y_lab[y_lab_predict == 9]))

y_custom = np.full(y_unlab_predict.shape, np.nan)
y_custom[y_unlab_predict == 0] = 6
y_custom[y_unlab_predict == 1] = 1
y_custom[y_unlab_predict == 2] = 0
y_custom[y_unlab_predict == 3] = 7
y_custom[y_unlab_predict == 4] = 8
y_custom[y_unlab_predict == 5] = 9
y_custom[y_unlab_predict == 6] = 3
y_custom[y_unlab_predict == 7] = 2
y_custom[y_unlab_predict == 8] = 4
y_custom[y_unlab_predict == 9] = 5
#y = np.append(y_lab, y_custom)
#print(y.shape)
#print(np.unique(y, return_counts=True))

y_lab = keras.utils.to_categorical(y_lab,n_labels) 
y_custom = keras.utils.to_categorical(y_custom,n_labels)

# NN
NN = Sequential()
NN.name = 'codersNN'

# settings
batch_size = 200
epochs = 500 # number of times to loop over whole data # shuffle when >1

learning_rate = 0.001
momentum = 0.9
decay = 1e-6

# optimizer
optimizer = SGD(lr=learning_rate, decay=decay, momentum=momentum)

# callback
monitor = 'val_acc'
earlyStopping = keras.callbacks.EarlyStopping(monitor=monitor, patience=25,
        verbose=1, mode='auto')

# set up neural network
NN.add(Dense(1000, activation='relu', input_shape=(pca_n_predictors,)))
NN.add(Dropout(0.5))
NN.add(Dense(500, activation='relu'))
NN.add(Dropout(0.5))
NN.add(Dense(300, activation='relu'))
NN.add(Dropout(0.5))
NN.add(Dense(50, activation='relu'))
#NN.add(Dropout(0.1))
NN.add(Dense(30, activation='relu'))
NN.add(Dense(10, activation='softmax'))

# set up model with settings
NN.compile(loss='categorical_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])

# fit model
NN.fit(X_unlab, y_custom, batch_size=batch_size, epochs=epochs, verbose=1,
        validation_data=(X_lab, y_lab), callbacks=[earlyStopping], 
        class_weight='auto', shuffle=True)

# evaluate it on the test data
score = NN.evaluate(X_lab, y_lab, verbose=0)

print('loss {}, accuracy {}'.format(score[0], score[1]))

# predict labels
y_sub = NN.predict(X_sub)
y_sub = np.argmax(y_unlab, axis=1)

# save submission file
output = np.zeros((X_sub.shape[0],2))
output[:,0] = sub_idx
output[:,1] = y_sub
