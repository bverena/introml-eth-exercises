#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from scipy.stats import mode
from sklearn.semi_supervised import label_propagation
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score, make_scorer
accuracy = make_scorer(accuracy_score)

# settings
n_folds = 5
pca_n_predictors = 25

# load data
print('Load')
train_lab = pd.read_hdf("train_labeled.h5", "train")
train_unlab = pd.read_hdf("train_unlabeled.h5", "train")
sub = pd.read_hdf("test.h5", "test")
print(train_lab.shape)
print(train_unlab.shape)
print(sub.shape)

# divide into test, train, X, y
y_lab = train_lab.values[:,0] 
X_lab = train_lab.values[:,1:]
X_unlab = train_unlab.values[:,:] # unlabeled

sub_idx = sub.index.values
X_sub = sub.values

X = np.append(X_lab, X_unlab, axis=0) # all train data
y = np.append(y_lab, -np.ones(X_unlab.shape[0]))

# Interaction terms TODO
#pf = PolynomialFeatures(degree=poly_degree, interaction_only=interaction_only)
#X = pf.fit_transform(X)
#X_sub = pf.transform(X_sub)
#X_lab = pf.transform(X_lab)

# scale
print('Scale')
scaler = StandardScaler()
#X = scaler.fit_transform(X)
X = scaler.fit_transform(X)
X_unlab = scaler.transform(X_unlab)
X_lab = scaler.transform(X_lab)
X_sub = scaler.transform(X_sub)

# dimensionality reduction 
print('PCA')
pca = PCA(pca_n_predictors, whiten=False)
X = pca.fit_transform(X)
X_unlab = pca.transform(X_unlab)
X_sub = pca.transform(X_sub)
X_lab = pca.transform(X_lab)
print(np.sum(pca.explained_variance_ratio_))

# cross-validation for label spreading parameter
print('CV on alpha')
alpha_range = np.arange(0.1,0.9,0.1)
n_neigh_range = np.arange(6,11,2)
param_grid = dict(n_neighbors=n_neigh_range, alpha=alpha_range)
#grid = GridSearchCV(label_propagation.LabelSpreading(kernel='knn', max_iter=200), param_grid={'alpha': alpha_range}, cv=n_folds, scoring=accuracy, verbose=2)
grid = GridSearchCV(label_propagation.LabelSpreading(kernel='knn', max_iter=200), param_grid=param_grid, cv=n_folds, scoring=accuracy, verbose=2)
grid.fit(X, y)

print("The best parameters are %s with a score of %0.2f"
      % (grid.best_params_, grid.best_score_))


# label spreading
label_spread = label_propagation.LabelSpreading(kernel='knn', alpha=0.8, max_iter=200)
label_spread.fit(X, y)
y_predict = label_spread.predict(X_sub)
print(y_predict.shape)
print(np.unique(y_predict, return_counts=True))
