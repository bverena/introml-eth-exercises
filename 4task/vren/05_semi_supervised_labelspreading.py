#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# import modules
import numpy as np
import pandas as pd
from sklearn.utils import class_weight
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.semi_supervised import label_spreading
from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold

sub_name = 'semiSup_labelspread'
rel_use = 1.00
n_folds = 5
n_jobs = -1
max_iter = 200
kernel = 'rbf'
if kernel == 'rbf':
    alpha_range = np.arange(0.2,0.6,0.8)
    gamma_range = np.logspace(-1.0, -0.7, 3)
elif kernel == 'knn':
    alpha_range = np.arange(0.05,0.2,0.1)
    n_neigh_range = np.arange(6,11,2)

i_submit = 1
alpha = 0.2
gamma = np.log(-0.7)


# import data
train_lab = pd.read_hdf("train_labeled.h5", "train")
train_unlab = pd.read_hdf("train_unlabeled.h5", "train")
sub = pd.read_hdf("test.h5", "test")

# labeled data
y_lab = train_lab.values[:,0] 
X_lab = train_lab.values[:,1:]
# reduce data set
split_ind = int(np.floor(rel_use*X_lab.shape[0]))
y_lab = y_lab[:split_ind]
X_lab = X_lab[:split_ind,:]

# unlabeled data
X_unlab = train_unlab.values[:,:]

# reduce data set
split_ind = int(np.floor(rel_use*X_unlab.shape[0]))
X_unlab = X_unlab[:split_ind,:]

# combine labeled and unlabeled
X = np.append(X_lab, X_unlab, axis=0)
y = np.full(X.shape[0], -1)
y[:len(y_lab)] = y_lab
print(X.shape)

# scale
scaler = StandardScaler().fit(X)
X_lab = scaler.transform(X_lab)
X_unlab = scaler.transform(X_unlab)


if i_submit:
    X = scaler.transform(X)

    if kernel == 'knn':
        label_spread = label_spreading.LabelSpreading(kernel='knn', alpha=alpha,
                                                        n_neighbors=param, n_jobs=n_jobs,
                                                        max_iter=max_iter)
    elif kernel == 'rbf':
        label_spread = label_spreading.LabeLSpreaing(kernel='rbf', gamma=gamma, alpha=alpha,
                                                        n_jobs=n_jobs,
                                                        max_iter=max_iter)

    label_spread.fit(X, y)

    # submission data
    X_sub = sub.values
    X_sub = scaler.transform(X_sub)

    y_predict = label_spread.predict(X_sub)

    # save submission file
    sub_idx = sub.index.values
    output = np.zeros((X_sub.shape[0],2))
    output[:,0] = sub_idx
    output[:,1] = y_predict

    np.savetxt('submission_{}.csv'.format(sub_name), output, fmt='%10.8f',
            delimiter=',', header = "Id,y", comments = '')
    quit()

# create folds of labeled
kf = KFold(n_splits=n_folds)


if kernel == 'knn':
    param_range = n_neigh_range
elif kernel == 'rbf':
    param_range = gamma_range

accs = np.full((n_folds, len(alpha_range), len(param_range)), np.nan)



for ai,alpha in enumerate(alpha_range):
    print('######### alpha ' + str(alpha))

    for pi,param in enumerate(param_range):
        print('### param ' + str(param))

        # learning model
        if kernel == 'knn':
            label_spread = label_propagation.LabelSpreading(kernel='knn', alpha=alpha,
                                                            n_neighbors=param, n_jobs=n_jobs,
                                                            max_iter=max_iter)
        elif kernel == 'rbf':
            label_spread = label_propagation.LabelSpreading(kernel='rbf', gamma=param, alpha=alpha,
                                                            n_jobs=n_jobs,
                                                            max_iter=max_iter)


        for fi, (cvtrain_idx, cvtest_idx) in enumerate(kf.split(X_lab)): #folds

            # select test and training fold
            X_cvtrain = X_lab[cvtrain_idx,:]
            y_cvtrain = y_lab[cvtrain_idx]

            X_cvtest = X_lab[cvtest_idx,:]
            y_cvtest = y_lab[cvtest_idx]

            # combine labeled and unlabeled
            X = np.append(X_cvtrain, X_unlab, axis=0)
            y = np.full(X.shape[0], -1)
            y[:len(y_cvtrain)] = y_cvtrain

            label_spread.fit(X, y)
            y_predict = label_spread.predict(X_cvtest)
            acc = accuracy_score(y_predict, y_cvtest)
            print(acc)
            accs[fi,ai,pi] = acc

print('########################################')
accs = np.mean(accs, axis=0)
print('alphas (downward) ' + str(alpha_range))
print('params (to the right) ' + str(param_range))
print(accs)


