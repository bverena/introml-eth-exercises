#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 1a: cross-validation on ridge regression
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports
import numpy as np
import matplotlib.pylab as plt
from sklearn.linear_model import Ridge
from sklearn.model_selection import KFold
#from sklearn.preprocessing import PolynomialFeatures
#from sklearn.pipeline import make_pipeline

# settings
lambdas = [0.1,1.,10.,100.,1000]
#lambdas = [0.1]
#perc_fold = 0.1
#degrees = np.arange(1,10,1) 
degrees = [1]
#print(degrees)

# load data
train = np.loadtxt('train.csv', skiprows=1, delimiter=',') 
sample = np.loadtxt('sample.csv', skiprows=1, delimiter=',')

# partition into x and y
ids = train[:,0]
y = train[:,1]
X = train[:,2:]
#print(X.shape)
#quit()

# create folds
n_folds = 10
kf = KFold(n_splits=n_folds)
kf.get_n_splits(X)

# initialize rmse array
rmse = np.zeros((n_folds, len(lambdas)))

# inspect data
print('train data shape: ', X.shape)
#plt.close()
#plt.plot(data_train[:,2:], label='xis')
#plt.plot(data_train[:,1], label='y')
#plt.legend()
#plt.show()

for li, l in enumerate(lambdas): # loop over lambdas

    print('lambda', l)

    for fi, (train_idx, test_idx) in enumerate(kf.split(X)): # loop over folds

        # select test and training fold
        X_train = X[train_idx,:]
        y_train = y[train_idx]

        X_test = X[test_idx,:]
        y_test = y[test_idx]

        # ridge regression without degree
        regr = Ridge(alpha=l,fit_intercept=False)
        regr.fit(X_train, y_train)
        y_predict = regr.predict(X_test)

        # calculate RMSE
        rmse[fi,li] = np.sqrt(1./len(y_predict)*np.sum((y_test - y_predict)**2))

rmse_mean = np.mean(rmse, axis=0)
print(rmse)
print(rmse_mean)
print(lambdas)
np.savetxt('submission_nointercept.csv', rmse_mean, fmt='%10.8f', delimiter='\n')








        # ridge regression with degree # SAME!
        #model = make_pipeline(PolynomialFeatures(1), Ridge(alpha=l))
        #model.fit(X_train, y_train)
        #y_predict2 = model.predict(X_test)
        #print(y_predict - y_predict2)

        #for count, degree in enumerate(degrees): # loop over degrees

        #    model = make_pipeline(PolynomialFeatures(degree), Ridge(alpha=l))
        #    model.fit(X_train, y_train)
        #    y_predict = model.predict(X_test)
        #    print(model.get_params())

        #    plt.close()
        #    plt.scatter(y_test, y_predict)
        #    plt.show()
        #    quit()


        ## initiate ridge regression object
        #regr = Ridge(alpha=l)

# write y_pred to file
#csv = np.vstack((id_test, y_pred)).T

#print('MAXIMUM VALUE IN PREDICTION DATA SET: ',np.max(np.abs(y_pred)))
#np.savetxt('submission.csv', csv, fmt=['%d','%20.16f'], delimiter=',', header='Id,y', comments='')
