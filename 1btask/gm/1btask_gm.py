#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 1b: cross-validation on ridge regression
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports
import numpy as np
from sklearn.linear_model import Ridge
from sklearn.model_selection import KFold
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline

# settings
lambdas = [0.1,1.,10.,100.,1000] + list(np.arange(1,50,5))
#lambdas = [10.,25,50]
lambdas = np.arange(10,20,1)
#lambdas = [10]

# load data
train = np.loadtxt('train.csv', skiprows=1, delimiter=',') 
sample = np.loadtxt('sample.csv', skiprows=0, delimiter=',')

# partition into x and y
ids = train[:,0]
y = train[:,1]
X = train[:,2:]


## transformed Features 
Features = np.zeros((ids.shape[0], 21))
# Linear
Features[:,0:5] = X
# Quadratic
Features[:,5:10] = X**2
# Exponential
Features[:,10:15] = np.exp(X)
# Cosine
Features[:,15:20] = np.cos(X)
# Constant
Features[:,20] = 1


# create folds
n_splits = 900
kf = KFold(n_splits=n_splits)
kf.get_n_splits(X)

# initialize rmse array
rmse = np.zeros((n_splits, len(lambdas)))

# inspect data
print('train data shape: ', train.shape)
print('sample data shape:', sample.shape)
#import matplotlib.pylab as plt
#plt.close()
#plt.plot(data_train[:,2:], label='xis')
#plt.plot(data_train[:,1], label='y')
#plt.legend()
#plt.show()

for li, l in enumerate(lambdas): # loop over lambdas

    print('lambda', l)
    print('#############')

    for fi, (train_idx, test_idx) in enumerate(kf.split(Features)): # loop over folds
        #print(np.round((fi+1)/n_splits,2))

        # select test and training fold
        X_train = Features[train_idx,:]
        y_train = y[train_idx]

        X_test = Features[test_idx,:]
        y_test = y[test_idx]

        # ridge regression without degree
        regr = Ridge(alpha=l,fit_intercept=False)
        regr.fit(X_train, y_train)
        y_predict = regr.predict(X_test)

        # calculate RMSE
        rmse[fi,li] = np.sqrt(1./len(y_predict)*np.sum((y_test - y_predict)**2))

rmse_mean = np.mean(rmse, axis=0)
print(lambdas)
print(rmse_mean)

##quit()
# Determined Lambda = 15
lambd = 15

regr = Ridge(alpha=lambd,fit_intercept=False)
regr.fit(Features,y)
#y_predict = regr.predict(Features)
#rmse = np.sqrt(1./len(y_predict)*np.sum((y - y_predict)**2))
weights = regr.coef_
print(weights)
np.savetxt('submission.csv', weights, fmt='%10.8f', delimiter='\n')



