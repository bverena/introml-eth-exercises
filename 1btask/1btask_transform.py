#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 1b: cross-validation on ridge regression
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports maths
import numpy as np
import matplotlib.pylab as plt

# imports machine learning
from sklearn.linear_model import Ridge
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Lasso
from sklearn.model_selection import KFold
#from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline

# settings
# options: 'linear', 'ridge', 'lasso'
regr_type = 'lasso'
n_folds = 900
i_save = True

if regr_type in ['ridge', 'lasso']:
    #lambdas = [0.1,1.,10.,100.,1000.]
    lambdas = np.arange(0.04,0.08,0.01)
elif regr_type is 'linear':
    lambdas = [None]

# load data
train = np.loadtxt('train.csv', skiprows=1, delimiter=',') 
sample = np.loadtxt('sample.csv', skiprows=0, delimiter=',')

# partition into x and y
ids = train[:,0]
y = train[:,1]
X = train[:,2:]

# transforming Features 
Features = np.zeros((ids.shape[0], 21))
Features[:,0:5] = X # linear
Features[:,5:10] = X**2 # quadratic
Features[:,10:15] = np.exp(X) # exponential
Features[:,15:20] = np.cos(X) # cosine
Features[:,20] = 1 # constant

# standardize
#scaler = StandardScaler().fit(Features)
#print(scaler.mean_)
#print(scaler.scale_)
##quit()
#Features = scaler.transform(Features)

# create folds
kf = KFold(n_splits=n_folds)
kf.get_n_splits(X)

# initialize rmse array
rmse = np.zeros((n_folds, len(lambdas)))

# inspect data
print('train data shape: ', X.shape)

for li, l in enumerate(lambdas): # lambdas

    print('#############')
    print('lambda', l)

    for fi, (train_idx, test_idx) in enumerate(kf.split(Features)): #folds

        # select test and training fold
        X_train = Features[train_idx,:]
        y_train = y[train_idx]

        X_test = Features[test_idx,:]
        y_test = y[test_idx]

        ## standardize
        #scaler = StandardScaler().fit(X_train)
        #X_train = scaler.transform(X_train)

        #X_test = scaler.transform(X_test)

        # initialize regression
        if regr_type == 'ridge':
            regr = Ridge(alpha=l)
        elif regr_type == 'linear':
            regr = LinearRegression()
        elif regr_type == 'lasso':
            regr = Lasso(alpha=l)
        else:
            raise AttributeError('regression type {} not understood.'.format(regr_type))
            
        # fit regression to training data
        regr.fit(X_train, y_train)

        # predict from fitted regression
        y_predict = regr.predict(X_test)

        # calculate rmse
        rmse[fi,li] = np.sqrt(1./len(y_predict)*np.sum((y_test - y_predict)**2))

    rmse_lambda = np.mean(rmse[:,li],0)
    print('rmse: ',str(np.round(rmse_lambda,3)))
    print('#############')
    if i_save:
        if regr_type == 'ridge':
            regr = Ridge(alpha=l)
        elif regr_type == 'linear':
            regr = LinearRegression()
        elif regr_type == 'lasso':
            regr = Lasso(alpha=l,fit_intercept=True)
        else:
            raise AttributeError('regression type {} not understood.'.format(regr_type))
        #Features = scaler.transform(Features)
        regr.fit(Features, y)
        weights = regr.coef_
        np.savetxt('submission_{}_f{}_l{}_rmse_{}.csv'.format(regr_type, n_folds,
                    np.round(l,2), np.round(rmse_lambda,3)), weights, fmt='%10.8f',
                    delimiter='\n')

rmse_mean = np.mean(rmse, axis=0)
print(lambdas)
print(rmse_mean)
#quit()


