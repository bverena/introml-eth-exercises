#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports maths
import numpy as np
import matplotlib.pylab as plt
from matplotlib.colors import Normalize
# imports machine learning
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, make_scorer
accuracy = make_scorer(accuracy_score)
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import GridSearchCV

# load train data
train = np.loadtxt('train.csv', skiprows=1, delimiter=',')
sample = np.loadtxt('sample.csv', skiprows=1, delimiter=',')
test = np.loadtxt('test.csv', skiprows=1, delimiter=',')

# partition into x (the features) and y (the labels)
ids_train = train[:,0]
y_train = train[:,1]
X_train = train[:,2:]

ids_test = test[:,0]
X_test = test[:,1:]

# standardisation
scaler = StandardScaler()
scaler.fit(np.concatenate((X_train, X_test)))
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

# train classifiers initial search
C_range = np.logspace(-2, 4, 7)
gamma_range = np.logspace(-4, 4, 7)
cf_range = np.logspace(-2, 4, 7)

# gamma_range = np.logspace(-3, 0, 4)
# C_range = np.logspace(-1, 3, 6)
gamma_range = np.linspace(0.01, 0.2, 10)
C_range = np.linspace(0.2, 2, 10)
# gamma_range = np.logspace(-10, -1, 10)
# C_range = np.logspace(-1, 7, 10)

polydeg = 3
splits = 5
kernel = 'poly'

param_grid = dict(gamma=gamma_range, C=C_range, coef0=cf_range)
cv = StratifiedShuffleSplit(n_splits=splits, test_size=int(X_train.shape[0]/splits))
grid = GridSearchCV(SVC(kernel=kernel, degree=polydeg, class_weight='balanced'),
                    param_grid=param_grid, cv=cv,
                    scoring=accuracy, verbose=2)
grid.fit(X_train, y_train)

print("The best parameters are %s with a score of %0.2f"
      % (grid.best_params_, grid.best_score_))


# #############################################################################
# Visualization

# Draw heatmap of the validation accuracy as a function of gamma and C
#
# The score are encoded as colors with the hot colormap which varies from dark
# red to bright yellow. As the most interesting scores are all located in the
# 0.92 to 0.97 range we use a custom normalizer to set the mid-point to 0.92 so
# as to make it easier to visualize the small variations of score values in the
# interesting range while not brutally collapsing all the low score values to
# the same color.

class MidpointNormalize(Normalize):

    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))


scores = grid.cv_results_['mean_test_score'].reshape(len(C_range), len(gamma_range))

plt.figure(figsize=(8, 6))
plt.subplots_adjust(left=.2, right=0.95, bottom=0.15, top=0.95)
plt.imshow(scores, interpolation='nearest', cmap=plt.cm.hot,
           norm=MidpointNormalize(vmin=0.2, midpoint=0.92))
plt.xlabel('gamma')
plt.ylabel('C')
plt.colorbar()
plt.xticks(np.arange(len(gamma_range)), gamma_range, rotation=45)
plt.yticks(np.arange(len(C_range)), C_range)
plt.title('Validation accuracy')
plt.show()


# apply on test dataset
C = grid.best_params_['C']
gamma = grid.best_params_['gamma']
acc = grid.best_score_

classif = SVC(kernel=kernel, degree=polydeg, C=C, gamma=gamma, class_weight='balanced')
classif.fit(X_train, y_train)
y_predict = classif.predict(X_test)

# create and save output matrix
output = np.zeros((X_test.shape[0], 2))
output[:,0] = ids_test
output[:,1] = y_predict

np.savetxt('submission_svc({})_acc_{}_f_{}_C_{}_g_{}_d_{}.csv'
           .format(kernel, acc, splits, np.round(C, 2), np.round(gamma, 2), polydeg), output,
           fmt='%10.8f', delimiter=',', header="Id,y", comments='')

