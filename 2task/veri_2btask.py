#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports maths
import numpy as np
import matplotlib.pylab as plt

# imports machine learning
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, make_scorer
accuracy = make_scorer(accuracy_score)
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import PolynomialFeatures

i_degree = 1
i_interactionOnly = True

# load train data
train = np.loadtxt('train.csv', skiprows=1, delimiter=',') 
sample = np.loadtxt('sample.csv', skiprows=1, delimiter=',')
test = np.loadtxt('test.csv', skiprows=1, delimiter=',') 

# partition into x (the features) and y (the labels)
ids_train = train[:,0]
y_train = train[:,1]
X_train = train[:,2:]

ids_test = test[:,0]
X_test = test[:,1:]

# POLYNOMIAL FEATURES
poly = PolynomialFeatures(i_degree, interaction_only=i_interactionOnly)
X_train = poly.fit_transform(X_train)
X_test = poly.transform(X_test)
print(X_test.shape)

# standardisation
scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.fit_transform(X_test)

# train classifiers initial search
#C_range = np.logspace(-2, 10, 13)
gamma_range = np.logspace(-9, 3, 13)
gamma_range = np.logspace(-7, -5, 5)
#C_range = np.logspace(-2, 5, 8)
C_range = np.logspace(-2, 10, 13)
C_range = np.logspace(3, 5, 5)
#gamm_range = np.logspace(-2, 5, 8)
param_grid = dict(gamma=gamma_range, C=C_range)
cv = StratifiedShuffleSplit(n_splits=5, test_size=0.10)
#grid = GridSearchCV(SVC(), param_grid=param_grid, cv=cv, verbose=2)
grid = GridSearchCV(SVC(), param_grid=param_grid, cv=cv, scoring=accuracy, verbose=2,
                    n_jobs=2)
grid.fit(X_train, y_train)

print("The best parameters are %s with a score of %0.2f"
      % (grid.best_params_, grid.best_score_))

# apply on test dataset
C = grid.best_params_['C']
gamma = grid.best_params_['gamma']
classif = SVC(C=C, gamma=gamma)
classif.fit(X_train, y_train)
y_predict = classif.predict(X_test)

# create and save output matrix
output = np.zeros((X_test.shape[0],2))
output[:,0] = ids_test
output[:,1] = y_predict

np.savetxt('submission_svc_f_{}_k_{}_C_{}_g_{}.csv'.format(5, 'gridsearch',
    np.round(C,2), np.round(gamma,2)), output, fmt='%10.8f',
    delimiter=',', header = "Id,y", comments = '')

############## OLD CODE
## create folds
#kf = KFold(n_splits=n_folds)
#kf.get_n_splits(X)
#
## initialize acc array
#acc = np.zeros((n_folds, len(lambdas)))
#
## inspect data
#print('train data shape: ', X.shape)
#
#for li, l in enumerate(lambdas): # lambdas
#
#    print('#############')
#    print('lambda', l)
#
#    for fi, (cvtrain_idx, cvtest_idx) in enumerate(kf.split(X)): #folds
#        print(fi)
#
#        # select test and training fold
#        X_cvtrain = X[cvtrain_idx,:]
#        y_cvtrain = y[cvtrain_idx]
#
#        X_cvtest = X[cvtest_idx,:]
#        y_cvtest = y[cvtest_idx]
#
#        # initialize regression
#        classif = LinearSVC(C=l,penalty='l2', loss='hinge')
#
#        # fit regression to training data
#        classif.fit(X_cvtrain, y_cvtrain)
#
#        # predict from fitted regression
#        y_predict = classif.predict(X_cvtest)
#
#        # calculate rmse
#        acc[fi,li] = accuracy_score(y_cvtest, y_predict)
#
#    acc_lambda = np.mean(acc[:,li],0)
#    print('acc: ',str(np.round(acc_lambda,4)))
#    print('#############')
#
## Print mean accuracy over all folds for each lambda
#acc_mean = np.mean(acc, axis=0)
#print(lambdas)
#print(acc_mean)
#
## This is our preferred lambda
#lambda_pref = 0.09
#classif = LinearSVC(C=lambda_pref,penalty='l2', loss='hinge')
## fit classification model to training data
#classif.fit(X, y)
#
#
##Load test data set
#test = np.loadtxt('test.csv', skiprows=1, delimiter=',')
#ids_test = test[:,0]
#X_test = test[:,1:]
## predict y_test from fitted classification model
#y_test_predict = classif.predict(X_test)
## Create output_matrix 
#output = np.zeros((X_test.shape[0],2))
#output[:,0] = ids_test
#output[:,1] = y_test_predict
#


    



