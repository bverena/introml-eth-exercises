#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports maths
import numpy as np
import matplotlib.pylab as plt

# imports machine learning
from sklearn.model_selection import KFold
#from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
#from sklearn.preprocessing import KernelCenterer
from sklearn.preprocessing import PolynomialFeatures

# settings
n_folds = 20
lambdas = [0.1,1,10,100,1000]
lambdas = np.logspace(-1,1,10)
tolerance = 1E-3
i_degree = 3
i_interactionOnly = True
lambdas = []
# CHOSEN LAMBDAS
#chosen_lambda = 10**(-0.77) 
chosen_lambda = 1


# load train data
train = np.loadtxt('train.csv', skiprows=1, delimiter=',') 
sample = np.loadtxt('sample.csv', skiprows=1, delimiter=',')


# partition into x (the features) and y (the labels)
ids = train[:,0]
y = train[:,1]
X = train[:,2:]

n_entries = X.shape[0]
n_predictors = X.shape[1]

# POLYNOMIAL FEATURES
poly = PolynomialFeatures(i_degree, interaction_only=i_interactionOnly)
X = poly.fit_transform(X)
print(X.shape)


scaler = StandardScaler()
X = scaler.fit_transform(X)




# create folds
kf = KFold(n_splits=n_folds)
kf.get_n_splits(X)

# initialize acc array
acc = np.zeros((n_folds, len(lambdas)))

# inspect data
print('train data shape: ', X.shape)

if len(lambdas) > 0:
    for li, l in enumerate(lambdas): # lambdas

        print('########################################')
        print('lambda', l)
        
        for fi, (cvtrain_idx, cvtest_idx) in enumerate(kf.split(X)): #folds
            #print(fi)

            # select test and training fold
            X_cvtrain = X[cvtrain_idx,:]
            y_cvtrain = y[cvtrain_idx]

            X_cvtest = X[cvtest_idx,:]
            y_cvtest = y[cvtest_idx]

            # initialize regression
            classif = LinearSVC(penalty='l1', dual=False, C=l,
                    class_weight='balanced')

            # fit regression to training data
            classif.fit(X_cvtrain, y_cvtrain)

            # predict from fitted regression
            y_predict = classif.predict(X_cvtest)

            # calculate accuracy
            accuracy = accuracy_score(y_cvtest, y_predict)
            acc[fi,li] = accuracy
        print(acc)

        acc_lambda = np.mean(acc[:,li],0)
        print('acc: ',str(np.round(acc_lambda,4)))
        print('#############')

    # Print mean accuracy over all folds for each lambda
    acc_mean = np.mean(acc, axis=0)
    print(np.log10(lambdas))
    print(acc_mean)
    print(np.max(acc_mean))

    quit()

classif = LinearSVC(penalty='l1', dual=False, C=chosen_lambda,
        class_weight='balanced')
# fit classification model to training data
classif.fit(X, y)


#Load test data set
test = np.loadtxt('test.csv', skiprows=1, delimiter=',')
ids_test = test[:,0]
X_test = test[:,1:]
X_test = poly.transform(X_test)
X_test = scaler.transform(X_test)
# predict y_test from fitted classification model
y_test_predict = classif.predict(X_test)
# Create output_matrix 
output = np.zeros((X_test.shape[0],2))
output[:,0] = ids_test
output[:,1] = y_test_predict

np.savetxt('submission_linearSVC_f_{}_l_{}_intOnly_{}.csv'.format(n_folds,
            np.round(chosen_lambda,2), i_interactionOnly), 
            output, fmt='%10.8f', delimiter=',', header = "Id,y", comments = '')


    



