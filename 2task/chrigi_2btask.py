#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports maths
import numpy as np
import matplotlib.pylab as plt

# imports machine learning
from sklearn.model_selection import KFold
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import KernelCenterer
#from sklearn.svm import NuSVC
#from sklearn.metrics.pairwise import chi2_kernel

# settings
n_folds = 5
i_save = True
i_fitInt = False
out_label = ''
# linear, poly, rbf, sigmoid
svc_kernel = "poly"
lambdas = [0.1,1,10,100,1000]
#lambdas = np.arange(0.2,0.5,0.1)
#lambdas = np.logspace(-1,4,16)
#lambdas = [5]
#lambdas = [0.1,0.5,1,5,10]
gammas = [0.0001,0.001,0.01,0.1]
#gammas = np.logspace(-6,1,16)
chosen_gamma = 0.05
chosen_lambda = 5
#max_iter = 10000
tolerance = 1E-3
degree = 3
#lambdas = [1]
#gammas = [5]


print('Use kernel: '+svc_kernel)
print('#########################')

# load train data
train = np.loadtxt('train.csv', skiprows=1, delimiter=',') 
sample = np.loadtxt('sample.csv', skiprows=1, delimiter=',')


# partition into x (the features) and y (the labels)
ids = train[:,0]
y = train[:,1]
X = train[:,2:]

n_entries = X.shape[0]
n_predictors = X.shape[1]

#import random
#X_new = np.zeros((n_entries,n_predictors*2))
#X_new[:,0:n_predictors] = X
#for i in range(n_predictors,2*n_predictors):
#    i0 = random.randint(0,15)
#    j0 = random.randint(0,15)
#    X_new[:,i] = X[:,i0]*X[:,j0]
#    X_new[:,i] = np.log(X[:,i0])
#X = X_new
#X[np.isnan(X)] = 0
#X[X == -np.Inf] = -99999
#X[X == np.Inf] = 99999


#scaler = StandardScaler()
scaler = KernelCenterer()
X = scaler.fit_transform(X)



#import random
#fig,axes = plt.subplots(4,4)
#for i in range(0,16):
#    #if i == 15:
#    #    j = 0
#    #else:
#    #    j = i + 1
#    rI = i // 4
#    cI = i - rI*4
#    ax = axes[rI,cI]
#
#    # SCATTER
#    i0 = random.randint(0,15)
#    i1 = random.randint(0,15)
#    ax.scatter(X[:,i0],X[:,i1],c=y)
#    ax.set_title(str(i0)+' '+str(i1))
#
#    ## 1D
#    #ax.scatter(X[:,i],y)
#    #ax.set_title(i)
#
#    ## SCATTER
#    #i0 = random.randint(0,15)
#    #i1 = random.randint(0,15)
#    #ax.scatter(X[:,i0]*X[:,i1],y,c=y)
#    #ax.set_title(str(i0)+' '+str(i1))
#plt.tight_layout()
#plt.show()
#quit()



# create folds
kf = KFold(n_splits=n_folds)
kf.get_n_splits(X)

# initialize acc array
acc = np.zeros((n_folds, len(lambdas), len(gammas)))

# inspect data
print('train data shape: ', X.shape)

for li, l in enumerate(lambdas): # lambdas

    print('########################################')
    print('lambda', l)
    
    for gi, g in enumerate(gammas): # gammas

        print('#############')
        print('gamma', g)

        for fi, (cvtrain_idx, cvtest_idx) in enumerate(kf.split(X)): #folds
            #print(fi)

            # select test and training fold
            X_cvtrain = X[cvtrain_idx,:]
            y_cvtrain = y[cvtrain_idx]

            X_cvtest = X[cvtest_idx,:]
            y_cvtest = y[cvtest_idx]

            # initialize regression
            classif = SVC(C=l,kernel=svc_kernel, tol=tolerance, degree=degree, gamma=g, cache_size=2000, class_weight='balanced')
            #classif = LinearSVC(penalty='l2', loss='hinge', dual=True, C=l, class_weight='balanced')
            #classif = NuSVC(nu=l,kernel=svc_kernel, tol=tolerance, degree=degree, gamma=g, cache_size=2000,
            #        class_weight='balanced')

            # fit regression to training data
            classif.fit(X_cvtrain, y_cvtrain)

            # predict from fitted regression
            y_predict = classif.predict(X_cvtest)

            # calculate rmse
            #index = li*len(gammas) + gi
            accuracy = accuracy_score(y_cvtest, y_predict)
            acc[fi,li,gi] = accuracy
        print(acc)

    acc_lambda = np.mean(acc[:,li],0)
    print('acc: ',str(np.round(acc_lambda,4)))
    print('#############')

# Print mean accuracy over all folds for each lambda
acc_mean = np.mean(acc, axis=0)
print(np.log10(lambdas))
print(np.log10(gammas))
print(acc_mean)
print(np.max(acc_mean))

#plt.pcolor(np.log(gammas),np.log(lambdas),acc_mean)
#plt.colorbar()
#plt.show()
quit()

# This is our preferred lambda
#lambda_pref = 10
#classif = SVC(C=lambda_pref,kernel= svc_kernel, degree=13, max_iter=max_iter, gamma=gamma)
classif = SVC(C=chosen_lambda,kernel= svc_kernel, degree=degree, tol=tolerance, gamma=chosen_gamma, class_weight='balanced')
# fit classification model to training data
classif.fit(X, y)


#Load test data set
test = np.loadtxt('test.csv', skiprows=1, delimiter=',')
ids_test = test[:,0]
X_test = test[:,1:]
X_test = scaler.transform(X_test)
# predict y_test from fitted classification model
y_test_predict = classif.predict(X_test)
# Create output_matrix 
output = np.zeros((X_test.shape[0],2))
output[:,0] = ids_test
output[:,1] = y_test_predict

np.savetxt('submission_svc_f_{}_k_{}_l_{}_g_{}.csv'.format(n_folds,
            svc_kernel, np.round(chosen_lambda,2), np.round(chosen_gamma,2)), 
            output, fmt='%10.8f', delimiter=',', header = "Id,y", comments = '')


    



