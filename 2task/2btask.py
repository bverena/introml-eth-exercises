#!/usr/bin/env python
# # -*- coding: utf-8 -*-

""" Lecture Introduction to Machine Learning, ETH Zurich
Spring Semester 2018
Project Task 2: classification
Group Yungcoders, @author: Christoph Heim (heimc) and Verena Bessenbacher (bverena) and Gianna Marano (gmarano)

Description:
TBC
"""

# imports maths
import numpy as np
import matplotlib.pylab as plt

# imports machine learning
from sklearn.model_selection import KFold
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score

# settings
n_folds = 5
i_save = True
i_fitInt = False
out_label = ''
# linear, poly, rbf, sigmoid
svc_kernel = "poly"
lambdas = [0.1,1.,10.,100.,1000.]
lambdas = [1.,10.,100.]
#lambdas = []


print('Use kernel: '+svc_kernel)
print('#########################')

# load train data
train = np.loadtxt('train.csv', skiprows=1, delimiter=',') 
sample = np.loadtxt('sample.csv', skiprows=1, delimiter=',')


# partition into x (the features) and y (the labels)
ids = train[:,0]
y = train[:,1]
X = train[:,2:]

print(np.histogram(y))
quit()

# create folds
kf = KFold(n_splits=n_folds)
kf.get_n_splits(X)

# initialize acc array
acc = np.zeros((n_folds, len(lambdas)))

# inspect data
print('train data shape: ', X.shape)

for li, l in enumerate(lambdas): # lambdas

    print('#############')
    print('lambda', l)

    for fi, (cvtrain_idx, cvtest_idx) in enumerate(kf.split(X)): #folds
        print(fi)

        # select test and training fold
        X_cvtrain = X[cvtrain_idx,:]
        y_cvtrain = y[cvtrain_idx]

        X_cvtest = X[cvtest_idx,:]
        y_cvtest = y[cvtest_idx]

        # initialize regression
        classif = SVC(C=l,kernel= svc_kernel)

        # fit regression to training data
        classif.fit(X_cvtrain, y_cvtrain)

        # predict from fitted regression
        y_predict = classif.predict(X_cvtest)

        # calculate rmse
        acc[fi,li] = accuracy_score(y_cvtest, y_predict)

    acc_lambda = np.mean(acc[:,li],0)
    print('acc: ',str(np.round(acc_lambda,4)))
    print('#############')

# Print mean accuracy over all folds for each lambda
acc_mean = np.mean(acc, axis=0)
print(lambdas)
print(acc_mean)

# This is our preferred lambda
lambda_pref = 100
classif = SVC(C=lambda_pref,kernel= svc_kernel)
# fit classification model to training data
classif.fit(X, y)


#Load test data set
test = np.loadtxt('test.csv', skiprows=1, delimiter=',')
ids_test = test[:,0]
X_test = test[:,1:]
# predict y_test from fitted classification model
y_test_predict = classif.predict(X_test)
# Create output_matrix 
output = np.zeros((X_test.shape[0],2))
output[:,0] = ids_test
output[:,1] = y_test_predict

np.savetxt('submission_svc_f_{}_k_{}_l_{}.csv'.format(n_folds,
            svc_kernel, np.round(lambda_pref,2)), output, fmt='%10.8f',
            delimiter=',', header = "Id,y", comments = '')


    



